// Topic: Decision making with match
//
// Program requirements:
// * Display "one", "two", "three", or "other" based on whether
//   the value of a variable is 1, 2, 3, or some other number,
//   respectively
//
// Notes:
// * Use a variable set to any integer
// * Use a match expression to determine which message to display
// * Use an underscore (_) to match on any value

fn main() {
    let my_num: i32 = 2;

    match my_num {
        1 => println!("The number is One"),
        2 => println!("The number is Two"),
        3 => println!("The number is three"),
        _ => println!("Others"),
    }
}
