// Topic: Implementing functionality with the impl keyword
//
// Requirements:
// * Print the characteristics of a shipping box
// * Must include dimensions, weight, and color
//
// Notes:
// * Use a struct to encapsulate the box characteristics
// * Use an enum for the box color
// * Implement functionality on the box struct to create a new box
// * Implement functionality on the box struct to print the characteristics

enum Color {
    Brown,
    Red,
    Green,
}

impl Color {
    fn print(&self) {
        match self {
            Color::Brown => println!("Brown"),
            Color::Red => println!("Red"),
            Color::Green => println!("Green"),
        }
    }
}

struct Dimensions {
    length: f64,
    breadth: f64,
    height: f64,
}

impl Dimensions {
    fn print(&self) {
        println!("Length= {:?}", self.length);
        println!("Breadth= {:?}", self.breadth);
        println!("Height= {:?}", self.height);
    }
}

struct BoxType {
    color: Color,
    weight: f64,
    dimensions: Dimensions,
}

impl BoxType {
    fn new(color: Color, weight: f64, dimensions: Dimensions) -> Self {
        Self {
            weight,
            color,
            dimensions,
        }
    }

    fn print(&self) {
        self.color.print();
        self.dimensions.print();
        println!("Weight: {:?}", self.weight);
    }
}

fn main() {
    let small_box_dimensions = Dimensions {
        length: 8.9,
        breadth: 7.0,
        height: 2.1,
    };

    let small_box = BoxType::new(Color::Brown, 56.5, small_box_dimensions);
    small_box.print();
}
