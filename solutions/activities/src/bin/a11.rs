// Topic: Ownership
//
// Requirements:
// * Print out the quantity and id number of a grocery item
//
// Notes:
// * Use a struct for the grocery item
// * Use two i32 fields for the quantity and id number
// * Create a function to display the quantity, with the struct as a parameter
// * Create a function to display the id number, with the struct as a parameter

struct GroceryItem {
    quantity: i32,
    item_id: i32,
}

fn print_quantity(item: &GroceryItem) {
    println!("The quantity is : {:?}", item.quantity);
}

fn print_item_id(item: &GroceryItem) {
    println!("The item ID is : {:?}", item_id);
}

fn main() {
    let grocery = GroceryItem {
        quantity: 98,
        item_id: 112,
    };
    print_quantity(&grocery);
    print_item_id(&grocery);
}
