// Topic: Functions
//
// Program requirements:
// * Displays your first and last name
//
// Notes:
// * Use a function to display your first name
// * Use a function to display your last name
// * Use the println macro to display messages to the terminal

fn firstname() {
    println!("The");
}

fn lastname() {
    println!("Rock");
}

fn main() {
    firstname();
    lastname();
}
