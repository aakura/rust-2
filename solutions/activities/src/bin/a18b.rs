// Topic: Result & the question mark operator
//
// Requirements:
// * Determine if an employee can access a building using a digital keycard
// * Employees that can access the building are:
//   * Maintenance crews
//   * Marketing department employees
//   * Managers
// * Other employees that work at the company are:
//   * Line supervisors
//   * Kitchen staff
//   * Assembly technicians
// * Ensure that terminated employees cannot access the building
//   regardless of their position
//
// Notes:
// * Use an enum to represent all types of employees
// * Use a struct to store the employee type and whether they are
//   still employed
// * Use a function that returns a Result to determine if the employee
//   may enter the building
// * Print whether the employee may access the building
//   * Must use a function that utilizes the question mark operator to do this

// * Use an enum to represent all types of employees
enum EmployeeType {
    AssemblyTech,
    KitchenStaff,
    LineSupervisors,
    Maintenance,
    Manager,
    Marketing,
}

enum EmployementStatus {
    Active,
    Terminated,
}

// * Use a struct to store the employee type and whether they are
//   still employed
struct Employee {
    employee_type: EmployeeType,
    employement_status: EmployementStatus,
}

// * Print whether the employee may access the building
fn try_access(employees: &Employee) -> Result<(), String> {
    match employees.employement_status {
        EmployementStatus::Terminated => Err("Employee is already terminated.".to_owned()),
        EmployementStatus::Active => Ok(()),
    };

    match employees.employee_type {
        EmployeeType::Maintenance => Ok(()),
        EmployeeType::AssemblyTech => Ok(()),
        EmployeeType::Manager => Ok(()),
        EmployeeType::Marketing => Ok(()),
        _ => Err("invalid".to_owned()),
    }
}

fn print_access(employees: &Employee) -> Result<(), String> {
    let access = try_access(employees)?;
    println!("Access OK");
    Ok(())
}

fn main() {
    let manager = Employee {
        employee_type: EmployeeType::Manager,
        employement_status: EmployementStatus::Active,
    };

    match print_access(&manager) {
        Err(e) => println!("Access Denied: {:?}", e),
        _ => (),
    }
}
