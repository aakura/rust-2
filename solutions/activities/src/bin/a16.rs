// Topic: Option
//
// Requirements:
// * Print out the details of a student's locker assignment
// * Lockers use numbers and are optional for students
//
// Notes:
// * Use a struct containing the student's name and locker assignment
// * The locker assignment should use an Option<i32>

// * Use a struct containing the student's name and locker assignment
// * The locker assignment should use an Option<i32>

struct Student {
    name: String,
    locker: Option<i32>,
}

fn main() {
    let details = Student {
        name: "Ankur".to_owned(),
        locker: Some(13),
    };

    println!("Student name: {:?}", details.name);

    match details.locker {
        Some(ans) => println!("locker: {:?}", ans),
        None => println!("locker: no locker assigned"),
    }
}
