// Topic: Result
//
// Requirements:
// * Determine if a customer is able to make a restricted purchase
// * Restricted purchases require that the age of the customer
//   is at least 21
//
// Notes:
// * Use a struct to store at least the age of a customer
// * Use a function to determine if a customer can make a restricted purchase
// * Return a result from the function
// * The Err variant should detail the reason why they cannot make a purchase
///Demo part
/// #[derive(Debug)]
/// enum MenuChoice {
///     MainMenu,
///     Start,
///     Quit,
/// }
///
/// fn get_choice(input: &str) -> Result<MenuChoice, String> {
///     match input {
///         "mainmenu" => Ok(MenuChoice::MainMenu),
///         "start" => Ok(MenuChoice::Start),
///         "quit" => Ok(MenuChoice::Quit),
///         _ => Err("invalid choice".to_owned()),
///     }
/// }
///
/// fn print_choice(choice: &MenuChoice) {
///     println!("choice = {:?}", choice);
/// }
///
/// fn main() {
///     let choice: Result<MenuChoice, _> = get_choice("quit");
///
///     match choice {
///         Ok(inner_choice) => print_choice(&inner_choice),
///         Err(e) => println!("Invalid Choice: {:?}", e),
///     }
/// }

struct Customer {
    age: i32,
}

fn try_purchase(customer: Customer) -> Result<(), String> {
    if customer.age >= 21 {
        Ok(())
    } else {
        Err("Customer must be at least 21 years of age.".to_owned())
    }
}

fn main() {
    let om = Customer { age: 20 };
    let purchased = try_purchase(om);
    println!("{:?}", purchased);
}
