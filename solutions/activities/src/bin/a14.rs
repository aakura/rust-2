// Topic: Strings
//
// Requirements:
// * Print out the name and favorite colors of people aged 10 and under
//
// Notes:
// * Use a struct for a persons age, name, and favorite color
// * The color and name should be stored as a String
// * Create and store at least 3 people in a vector
// * Iterate through the vector using a for..in loop
// * Use an if expression to determine which person's info should be printed
// * The name and colors should be printed using a function

struct Person {
    age: i32,
    name: String,
    fav_color: String,
}

fn print_data(data: &str) -> &str {
    //println!("{:?}", data);
    data
}

fn main() {
    let people = vec![
        Person {
            age: 15,
            name: String::from("Arun"),
            fav_color: String::from("Yellow"),
        },
        Person {
            age: 5,
            name: String::from("Om"),
            fav_color: String::from("Blue"),
        },
        Person {
            age: 9,
            name: String::from("Samir"),
            fav_color: String::from("Red"),
        },
    ];

    for iterator in people {
        if iterator.age <= 10 {
            println!("The age is : {:?}", iterator.age);
            println!("Name: {:?}", print_data(&iterator.name));
            println!("Favorite Color: {:?}", print_data(&iterator.fav_color));
        }
    }
}
