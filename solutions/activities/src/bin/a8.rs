// Topic: Organizing similar data using structs
//
// Requirements:
// * Print the flavor of a drink and it's fluid ounces
//
// Notes:
// * Use an enum to create different flavors of drinks
// * Use a struct to store drink flavor and fluid ounce information
// * Use a function to print out the drink flavor and ounces
// * Use a match expression to print the drink flavor

enum DrinkFlavor {
    Apple,
    Mango,
    Orange,
}

struct DrinkType {
    flavor: DrinkFlavor,
    fluid_oz: f64,
}

fn drink_order(drink: DrinkType) {
    match drink.flavor {
        DrinkFlavor::Apple => println!("Apple"),
        DrinkFlavor::Mango => println!("Mango"),
        DrinkFlavor::Orange => println!("Orange"),
    }

    println!("Oz: {:?}", drink.fluid_oz);
}

fn main() {
    let orange = DrinkType {
        flavor: DrinkFlavor::Orange,
        fluid_oz: 2.99,
    };

    drink_order(orange);

    let mango = DrinkType {
        flavor: DrinkFlavor::Mango,
        fluid_oz: 6.87,
    };

    drink_order(mango);

    let apple = DrinkType {
        flavor: DrinkFlavor::Apple,
        fluid_oz: 2.0,
    };

    drink_order(apple);

    println!("Are you done?");
}
